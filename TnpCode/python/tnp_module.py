# input file root://xrootd-cms.infn.it//store/data/Run2022F/Muon/NANOAOD/22Sep2023-v2/80000/f28e42ee-9ce7-4554-883c-8dba709a2932.root from
# /Muon/Run2022F-22Sep2023-v2/NANOAOD

import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
from ROOT import TLorentzVector

import os
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection 
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module


class simple_tag_and_probe(Module):
    def __init__(self):
        # initialize tefficiencies
        self.tightEff =ROOT.TEfficiency("tightEff", "", 50,25,100)
        self.mediumEff=ROOT.TEfficiency("mediumEff", "", 50,25,100)
        self.mll=ROOT.TH1F("mll", "", 50,0,200)

    def endJob(self):
        # save output 
        tf=ROOT.TFile.Open("output.root", "recreate")
        tf.WriteTObject( self.tightEff )
        tf.WriteTObject( self.mediumEff )
        tf.WriteTObject( self.mll )
        tf.Close()
        
    def analyze(self, event):
        # its called once per event

        # the event must have passed a muon trigger
        if not event.HLT_IsoMu24: return 
        
        # we take all muons in the event
        muons = [x for x in Collection( event, "Muon") ]

        # get muons that have fired the trigger, we will need them later
        trigObj  = Collection(event, 'TrigObj')
        selTrigObj = [x for x in trigObj if abs(x.id) == 13 and (x.filterBits & 2) and (x.filterBits & 8) ] # bits 1 and 3 are Iso and Mu, respectively

        tagSelection = lambda muo : muo.tightId and muo.pfRelIso04_all and muo.pt > 25
        probeSelection = lambda muo : muo.looseId and muo.pt > 20 # which selection should i have? 
        
        for tag in muons:
            if not tagSelection( tag ): continue
            # we need to make sure the tag fires the trigger
            isTriggering=False
            for obj in selTrigObj:
                vl = ROOT.TLorentzVector(); vl.SetPtEtaPhiM( obj.pt, obj.eta, obj.phi, 0)
                if (tag.p4().DeltaR( vl ) < 0.4): 
                    isTriggering=True
                    break
            if not isTriggering: continue

            for probe in muons:
                if tag == probe: continue
                if not probeSelection(probe): continue
                # you continue from here :)  


tnp_module = lambda : simple_tag_and_probe() 
