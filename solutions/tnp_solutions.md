# Solutions

The resulting efficiencies when considering probe muons passing `looseId` and `pt > 20` GeV is the following 

![Part 1](tnp_nocuts.png "Part1")

This is around 5% less than what it is usually measured. If you check the $m_{\ell\ell}$ distribution, you will see that there are other contributions besides the Drell-Yan, specifically at low invariant mass.


Part of this can be due to genuine muons from low-mass resonances, but there's also a contribution of fake leptons. This is often handled by fitting the mll distribution and considering only the Z boson component. For a quick check, we can instead require the probe to pass the tight isolation requirement and also restrict ourselves to invariant masses closer than 20 GeV to the Z boson mass. By doing that, we get these efficiences, which are closer to the reference measurement. 

![Part 1](tnp_iso_mll.png "Part1")


Note that in these measurements, devil is in the details: the way background is handled matters, but also a wrongly-chosen selection for the probe, the tag or the whole event may result in a bias. 

