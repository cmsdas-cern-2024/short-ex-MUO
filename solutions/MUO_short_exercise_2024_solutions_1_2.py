import ROOT

ROOT.gSystem.Load("libFWCoreFWLite.so") # FWLite allows root to identify EDM files, like miniAOD or AOD
ROOT.gSystem.Load("libDataFormatsFWLite.so")
ROOT.FWLiteEnabler.enable()

from DataFormats.FWLite import Handle, Events

events = Events("dymm.root")

#define the collections to read, these are the same that you obtained running edmDumpEventContent
recMuonLabel = "slimmedMuons"
recMuons = Handle("std::vector<pat::Muon>")
genMuons = Handle("std::vector<pat::PackedGenParticle>")

# setup some counters
count_nTracker=0
count_nGlobal=0
count_nStandalone=0

# histograms to count the number of muons
hNTracker     = ROOT.TH1F("hNTracker", "", 6, -0.5, 5.5)
hNGlobal      = ROOT.TH1F("hNGlobal", "", 6, -0.5, 5.5)
hNStandalone  = ROOT.TH1F("hNStandalone", "", 6, -0.5, 5.5)


for i, event in enumerate(events):
    event.getByLabel(recMuonLabel, recMuons)
    count_per_event_nTracker=0
    count_per_event_nGlobal=0
    count_per_event_nStandalone=0

    for j, muon in enumerate(recMuons.product(),1):
        if muon.isTrackerMuon():
            count_nTracker+=1
            count_per_event_nTracker+=1
        if muon.isGlobalMuon():
            count_nGlobal+=1
            count_per_event_nGlobal+=1
        if muon.isStandAloneMuon() :
            count_nStandalone+=1
            count_per_event_nStandalone+=1

        hNTracker     .Fill( count_per_event_nTracker)
        hNGlobal      .Fill( count_per_event_nGlobal )
        hNStandalone  .Fill( count_per_event_nStandalone)

print( count_nTracker, count_nGlobal, count_nStandalone)
tf=ROOT.TFile.Open("output.root", "recreate")
hNTracker.Write()
hNGlobal.Write()
hNStandalone.Write()
tf.Close()
