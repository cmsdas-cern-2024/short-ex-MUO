 # Solution

The script to solve the exercise can be found [here](MUO_short_exercise_2024_solutions_1_3.py)

Overlay of (1), (2) and (3):

![Part 1](plot1.png "Part1")

At high momentum, most muons are included in the three collections, because all the two algorithms are very efficient at selecting muons well within the acceptance

At low momentum, there's a large contamination of both background muons (random matching of tracks and hits in the muons system) and significant contributions of signal muons for which the reconstruction algorithms are not that efficient. Low momentum muons don't traverse the full muon system and, instead leave hits in one or two layers. The tracker muon algorithm is more efficient at collecting those than the global reconstruction.

Overlay of (3) and (4)

![Part 2](plot2.png "Part2")

At high momentum there's more generated muons than reconstructed because in that momentum regime most muons in the sample are signal muons. However, although quite efficient, the global algorithm doesn't manage to reconstruct all the muons. 

At low momentum there's significant contribution of fake muons, that are not included in the simulation. Because, of this, we observe more reconstructed muons than generated ones.

