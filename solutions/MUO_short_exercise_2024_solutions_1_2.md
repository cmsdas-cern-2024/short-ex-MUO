   # Solutions
   
   
   1.  Looking at isTrackerMuon() we see more than 134654 muons. Looking at isGlobalMuon() we see 61141 muons. Looking at isStandAloneMuon() we see 70685 muons.
   2.  For a muon to be reconstructed as GlobalMuon it first need to be a StandaloneMuon, so the 61141 GlobalMuons have to be inside the 70685 StandaloneMuon. But in addition a GlobalMuon also needs to have a track in the tracker detector. It seems that only for 10% of the StandaloneMuon we were not able to find a proper track to build a GlobalMuon.
   3.  There is basically a factor of 2 between the number of TrackerMuon and the number of GlobalMuon. The two reconstructions are uncorrelated, they are parallel. A GlobalMuon does not have to be a TrackerMuon (even though most of the time a real muon actually passes both reconstructions). The TrackerMuon reconstruction is a looser reconstruction as it only requires a track in the tracker and segments in the muon station (but not standalone). It is much easier for a particle to be reconstructed as a TrackerMuon rather than to be a GlobalMuon. Hint: A lot of the trackerMuon may not be real muons.
   4. There are cases in which the events will have less than two reconstructed muons because of reconstruction inefficiencies or because one of the two muons may fall outside the acceptance. There are cases in which there are more than two muons present in the event: other than the muons from the DY process, there are muons produced in the “underlying event” or in pileup vertices (mostly from hadron decays) and they will appear inside the reconstructed muon collection. 


You can find the code in [this script](solutions/MUO_short_exercise_2024_solutions_1_2.py)



 