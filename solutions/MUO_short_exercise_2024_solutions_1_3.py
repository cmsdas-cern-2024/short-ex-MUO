import ROOT

ROOT.gSystem.Load("libFWCoreFWLite.so") # FWLite allows root to identify EDM files, like miniAOD or AOD
ROOT.gSystem.Load("libDataFormatsFWLite.so")
ROOT.FWLiteEnabler.enable()

from DataFormats.FWLite import Handle, Events

events = Events("dymm.root")

#define the collections to read, these are the same that you obtained running edmDumpEventContent
recMuonLabel = "slimmedMuons"
recMuons = Handle("std::vector<pat::Muon>")
genMuons = Handle("std::vector<pat::PackedGenParticle>")


all_muons       = ROOT.TH1F("all_muons"      , "", 200, 0, 100)
muons_tracker   = ROOT.TH1F("muons_tracker"  , "", 200, 0, 100)
muons_global    = ROOT.TH1F("muons_global"   , "", 200, 0, 100)
gen_muons       = ROOT.TH1F("gen_muons"      , "", 200, 0, 100)
gen_promptmuons = ROOT.TH1F("gen_promptmuons", "", 200, 0, 100)





for i, event in enumerate(events):
    event.getByLabel(recMuonLabel, recMuons)
    for j, muon in enumerate(recMuons.product(),1):
        if muon.pt() < 5 or abs(muon.eta())>2.4:
            continue
        all_muons.Fill( muon.pt() )
        if muon.isTrackerMuon():
            muons_tracker.Fill( muon.pt() )
        if muon.isGlobalMuon():
            muons_global.Fill( muon.pt())

    event.getByLabel("packedGenParticles", genMuons)

    for j, gen in enumerate(genMuons.product(),1):
        if ( abs(gen.pdgId()) != 13 ): continue
        gen_muons.Fill( gen.pt() )
        if gen.status() != 1 or not gen.isPromptFinalState(): continue
        gen_promptmuons.Fill( gen.pt() )


outfile = ROOT.TFile.Open("output.root","recreate")
all_muons      .Write()
muons_tracker  .Write()
muons_global   .Write()
gen_muons      .Write()
gen_promptmuons.Write()

outfile.Close()