# 2024 CMS Data Analysis School @ CERN: Muon Short Exercise
 
## Slides and mattermost:

Slides of the course can be found [here](https://docs.google.com/presentation/d/1QDWWYxY0swK2GV2zKqyGJOnS480AABAkfMCViozh4EQ/edit?usp=sharing)

Join the short exercise Mattermost channel cliking on [this link](https://mattermost.web.cern.ch/cmsdas24/channels/muo-short-exercise)


## Facilitators:

1. Sergio Sanchez Cruz. CERN [sergio.sanchez.cruz@cern.ch](mailto:sergio.sanchez.cruz@cern.ch)
2. Georgios Karathanasis. U. Colorado [georgios.karathanasis@cern.ch](mailto:georgios.karathanasis@cern.ch)
3. Nicolo Trevisani. KIT [nicolo.trevisani@cern.ch](mailto:nicolo.trevisani@cern.ch)

Material based on CMS DAS exercises by Tamer Elkafrawy and Norbert Neumeister.


## Introduction:

This short exercise will start with an overview of basic muon properties and how muons interact with matter. We will follow this with a discussion of the muon detector technology employed by CMS and how we use this information to reconstruct muon candidates. The discussion will finish by briefly looking at where muons come from and why they are interesting for physics analysis. The remainder of the time will be spent developing familiarity with accessing muon information in miniAOD files. We'll use this to look in detail at identification and isolation criteria for muons and conclude with brief discussions of other relevant topics including momentum scale corrections and determining trigger and selection efficiency.

## Setting up the workarea

Create and set up a CMSSW working area:

```
cmsrel CMSSW_14_0_6
cd CMSSW_14_0_6/src
git clone https://gitlab.cern.ch/cmsdas-cern-2024/short-ex-MUO.git CMSDAS
cmsenv
scramv1 b -j4
```

Copy the input file into your local area

```# to move it somewhere central
cp /eos/user/c/cmsdas/2024/short-ex-muo/dymm.root .             # this is MC
cp /eos/user/c/cmsdas/2024/short-ex-muo/singlemuon-2022.root .  # this is 2022 data, single muon events
```

## Exercise 1: Introduction, muon object and main variables

In this exercise we will get familiar with the muon objects in a `miniAOD` file. There are two other formats that you might use when doing an analysis: `NanoAOD` (a flat tree containing only the final and most general muon information) and `AOD` (similar to `miniAOD` but containing much more raw level information in order to allow analyzers to perform some of the reconstruction steps by themselves). 

Looking first at `miniAOD` will give you good knowledge and will allow you to be more flexible in case you want to design your own muon selection in the future. The input file you have already copied is a sample of Drell-Yan dimuon events generated Next-to-Leading-Order with the program `MadGraph` software in `MINIAOD`

### Step 1: Getting familiar with the muon information in `miniAOD`

You can start taking a look at the content of the file

```
edmDumpEventContent dymm.root
``` 

You should get something similar to what is shown in  [the solutions](solutions/MUO_short_exercise_2024_solutions_1_1.md)

You can read the [`miniAOD` workbook](https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookMiniAOD2017) to learn about the different collections of physics objects (not only muons!!)

### Question 1: 

Which collections contain muons or muon candidates? (At any level: generated, reconstructed, trigger, etc.) Among these collections which ones correspond to the final muon candidate?

You can find the solutions [here](solutions/MUO_short_exercise_2024_solutions_1_1.md)

### Step 2: Exploring the muon object in `miniAOD` 

Muons in `miniAOD` are stored in the `pat::Muon` object. You can explore find the methods `pat::Muon` in the class by exploring:

+ the  [`pat::Muon` header file](https://github.com/cms-sw/cmssw/blob/master/DataFormats/PatCandidates/interface/Muon.h) in `CMSSW`
+ the [`reco::Muon` header file](https://github.com/cms-sw/cmssw/blob/master/DataFormats/MuonReco/interface/Muon.h) in `CMSSW`. Note that `pat::Muon` inherits all the methods from `reco::Muon`, so if you don't find you favourite method in the `pat::Muon`header, it may be in the `reco::Muon`header!!

### Step 3: Selecting muons in `miniAOD`

You can loop over the muons in each event in the input file using this code snippet 
```
import ROOT

ROOT.gSystem.Load("libFWCoreFWLite.so") # FWLite allows root to identify EDM files, like miniAOD or AOD
ROOT.gSystem.Load("libDataFormatsFWLite.so")
ROOT.FWLiteEnabler.enable()

from DataFormats.FWLite import Handle, Events

events = Events("dymm.root")

#define the collections to read, these are the same that you obtained running edmDumpEventContent
recMuonLabel = "slimmedMuons"
recMuons = Handle("std::vector<pat::Muon>")
genMuons = Handle("std::vector<pat::PackedGenParticle>")

# setup some counters
count_nTracker=0
count_nGlobal=0
count_nStandalone=0


for i, event in enumerate(events):
    event.getByLabel(recMuonLabel, recMuons)

    for j, muon in enumerate(recMuons.product(),1):
        if muon.isTrackerMuon(): count_nTracker=count_nTracker+1
        write more code here to count the number of global and standalone muons

```


### Question 2: 

1. How many muons are reconstructed as TrackerMuon? as GlobalMuon? as StandaloneMuon? (approximately). Hint: the variable isTrackerMuon () is a boolean, it takes the value 1 when the muon is a TrackerMuon.
2. What can you deduce from the number of StandaloneMuon and GlobalMuon?
3. Why is there such a bigger number of muon reconstructed as TrackerMuon?
4. How many muons of each kind are there in each event? Since you are running on a Drell-Yan sample, you expect two reconstructed muons. Why do you see more in some cases and less in others? 

You can find the solutions [here](solutions/MUO_short_exercise_2024_solutions_1_2.md)

### Question 3:

Extending the script above, plot the the $p_T$ and $\eta$ of muons passing the following selections (pick a $p_T$ range between 0 and 500 GeV):

1. all `pat::Muon` objects

2. all `pat::Muon` objects with $p_T > 20$ GeV and $|\eta| < 2.4$

3. all `pat::Muon` objects with $p_T > 20$ GeV and $|\eta| < 2.4$, that are reconstructed as tracker muons

4. all `pat::Muon` objects with $p_T > 20$ GeV and $|\eta| < 2.4$, that are reconstructed as global muons

5. all final-state muons in the `reco::GenParticle` collection

6. all final-state muons in the `reco::GenParticle` collection with $p_T$ > 20 GeV and $|\eta|$ < 2.4, and not coming from a hadron or $\tau$ decay. Hint select final state particles with `status()==1` and particles not coming from from hadron or $\tau$ decays with `isPromptFinalState()`.

Overlay the different distributions and discuss how the different cuts are selecting the different sources of muons.

You can find the solutions [here](solutions/MUO_short_exercise_2024_solutions_1_3.md).


## Exercise 2: Tag-and-probe efficiencies using 2022 data 

Muon selection efficiencies are often measured with the tag-and-probe method. In the tag-and-probe method, we aim to select Drell-Yan events. We

+ first select a muon with a tight identification criterion (we make sure that this is indeed a muon, the tag). This muon must have fired the trigger
+ we select another muon with a looser selection (the probe): this is going to be the denominator of our efficiency measurement
+ our efficiency is the proportion of times the probe passes the efficiency we want to measure



We will measure the efficiency of tight and medium IDs in 2022 data. For this exercise, we will use `NanoAOD` data and the `nanoAOD-tools` module, which is integrated in `CMSSW`. For this exercise: 

1. Edit the example code  (see below) so that you fill the `TEfficiencies` appropriately .
2. Are the efficiencies that you obtain consistent with what is seen in the lecture? Why not? Check the $m_{\ell\ell}$ distribution! 
3. Consider applying isolation cuts to the probe selection and consider applying an mll cut to the tag-probe pair. How do the results change? 


You will find the example code under `CMSDAS/TnpCode/python/tnp_module.py`. To run the code once you have edited it, you can run the following command (make sure you have ran `cmsenv` and `scramv1 b -j4` before)

```
nano_postproc.py output singlemuon-2022.root -I CMSDAS.TnpCode.tnp_module tnp_module
```

You can find the solutions [here](solutions/tnp_solutions.md).
